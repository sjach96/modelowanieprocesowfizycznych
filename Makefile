CC=gcc
CPP=g++
FC = gfortran

CFLAGS=-c -O3 -Wall -fopenmp
FFLAGS=-c -O3 -Wall -fopenmp
LFLAGS= -Lout -lpso -lnorms -lgsl -lgslcblas -lm -fopenmp


lib:	out/pso.o out/norms.o
	ar -cvq out/libpso.a out/pso.o
	ar -cvq out/libnorms.a out/norms.o

example:	out/main.o
	mkdir -p out/bin
	$(CC) out/main.o $(LFLAGS) -o out/bin/example

example2:	out/main2.o out/ffunc.o
	mkdir -p out/bin
	$(CC) out/main2.o out/ffunc.o  $(LFLAGS) -o out/bin/example2

example3:	out/main_imp.o out/cfunc.o out/ffunc.o
	mkdir -p out/bin
	$(FC) out/main_imp.o out/cfunc.o out/ffunc.o  $(LFLAGS) -o out/bin/example3

sjach: out/sjach.o
	mkdir -p out/bin
	make lib
	$(CC) out/sjach.o $(LFLAGS) -o out/bin/sjach

laplace: out/laplace.o
	$(CPP) out/laplace.o src/EasyBMP.h src/EasyBMP.cpp $(LFLAGS) -o out/bin/laplace

out/norms.o:	src/norms.c
	mkdir -p out
	$(CC) $(CFLAGS) src/norms.c -o out/norms.o

out/pso.o:	src/pso.c
	mkdir -p out
	$(CC) $(CFLAGS) src/pso.c -o out/pso.o

out/main.o:	src/main.c
	mkdir -p out
	$(CC) $(CFLAGS) src/main.c -o out/main.o

out/ffunc.o:	src/ffunc.f95
	mkdir -p out
	$(FC) $(FFLAGS) src/ffunc.f95 -o out/ffunc.o

out/cfunc.o:	src/cfunc.c
	mkdir -p out
	$(CC) $(CFLAGS) src/cfunc.c -o out/cfunc.o

out/main2.o:	src/main2.c
	mkdir -p out
	$(CC) $(CFLAGS) src/main2.c -o out/main2.o

out/main_imp.o:	src/main_imp.f95
	mkdir -p out
	$(FC) $(FFLAGS) src/main_imp.f95 -o out/main_imp.o

out/sjach.o: src/sjach.c
	mkdir -p out
	$(CC) $(CFLAGS) src/sjach.c -o out/sjach.o 

out/laplace.o: src/laplace.c
	mkdir -p out
	$(CPP) $(CFLAGS) src/laplace.c -o out/laplace.o 

clean:
	rm -rf out
