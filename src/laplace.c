#include "EasyBMP.h"
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <stdio.h>
#include <getopt.h>
#include <string.h>

enum {
    TOP = 1,
    BOTTOM = 2,
    RIGHT = 3,
    LEFT = 4,
};

struct Points {
    int x;
    int y;
    int temperature;
};

struct Hip {
    struct Points point;
    RGBApixel color;
    float prev_temp;
};

struct LaplaceInput {
    int rows_number;
    int column_number;
    int corner;
    struct Points* points;
    int points_number;
    int temperature;
};


static int found_source(struct Hip* hip, struct LaplaceInput* lap)
{
    if(hip->point.x == lap->points[0].x && hip->point.y == lap->points[0].y)
        return 1;
//	for(int i = 0; i < lap->points_number; i++)
//	{
//		if(hip->point.x == lap->points[i].x && hip->point.y == lap->points[i].y)
//			return 1;
//	}
    return 0;
}

static RGBApixel hsv_to_rgb(unsigned char h, unsigned char s, unsigned char v)
{
    RGBApixel rgb;
    unsigned char region, remainder, p, q, t;

    if (s == 0)
    {
        rgb.Red = v;
        rgb.Green = v;
        rgb.Blue = v;
        return rgb;
    }

    region = h / 43;
    remainder = (h - (region * 43)) * 6;

    p = (v * (255 - s)) >> 8;
    q = (v * (255 - ((s * remainder) >> 8))) >> 8;
    t = (v * (255 - ((s * (255 - remainder)) >> 8))) >> 8;

    switch (region)
    {
        case 0:
            rgb.Red = v; rgb.Green = t; rgb.Blue = p;
            break;
        case 1:
            rgb.Red = q; rgb.Green = v; rgb.Blue = p;
            break;
        case 2:
            rgb.Red = p; rgb.Green = v; rgb.Blue = t;
            break;
        case 3:
            rgb.Red = p; rgb.Green = q; rgb.Blue = v;
            break;
        case 4:
            rgb.Red = t; rgb.Green = p; rgb.Blue = v;
            break;
        default:
            rgb.Red = v; rgb.Green = p; rgb.Blue = q;
            break;
    }

    return rgb;
}

static RGBApixel get_color(int low, int high, unsigned char value)
{
    return hsv_to_rgb(value, 255, 255);
}

static void move_point(struct Points* point, int max_width, int max_height)
{
    int flag = 1;
    while(flag)
    {
        int dir = rand() % 4 + 1;
        switch(dir)
        {
            case TOP:
                if(point->y + 2 < max_height)
                {
                    point->y += 1;
                    flag = 0;
                }
                break;
            case BOTTOM:
                if(point->y - 2 > 0)
                {
                    point->y -= 1;
                    flag = 0;
                }
                break;
            case RIGHT:
                if(point->x + 2 < max_width)
                {
                    point->x += 1;
                    flag = 0;
                }
                break;
            case LEFT:
                if(point->x - 2 > 0)
                {
                    point->x -= 1;
                    flag = 0;
                }
                break;
            default:
                fprintf(stderr, "ERROR: Something gone wrong!\n");
                exit(EXIT_FAILURE);
        }
    }
}

static void move_point_hip(struct Hip* hip, int max_width, int max_height, float** vect)
{
    if (vect[hip->point.x][hip->point.y] >= 5)
    {
        for (int i = -1; i <= 1; i++)
        {
            for (int j = -1; j <= 1; j++)
            {
                if(hip->point.x + i <= 0 || hip->point.x + i > max_width || hip->point.y + j <= 0 || hip->point.y + j > max_height)
                    continue;

                if(vect[hip->point.x + i][hip->point.y + j] > vect[hip->point.x][hip->point.y])
                {
                    hip->point.x = hip->point.x + i;
                    hip->point.y = hip->point.y + j;
                    return;
                }
            }
        }
    }


    if(hip->point.x >= max_width - 15)
    {
        hip->point.x = 15;
        if(hip->point.y >= max_height - 15)
        {
            hip->point.y = 15;
        }
        else
        {
            hip->point.y += 15;
        }
    }
    else
    {
        hip->point.x += 15;
    }
}

static void move_laplace_points(struct LaplaceInput* lap)
{
    for(int i = 0; i < lap->points_number; i++)
    {
        move_point(&lap->points[i], lap->rows_number, lap->column_number);
    }
}

static void setup_initial_temp(float** vector, struct LaplaceInput* lap)
{
    vector[lap->points[0].x][lap->points[0].y] = 100;

    for(int i = 1; i < lap->points_number; i++)
    {
        vector[lap->points[i].x][lap->points[i].y] = lap->points[i].temperature;
    }
}

static void prepare_vector_random(float** vect, struct LaplaceInput* laplace)
{
    for(int i = 0; i < laplace->rows_number; i++)
    {
        for(int j = 0; j < laplace->column_number; j++)
        {
            vect[i][j] = 0;
        }
    }

    for(int i = 0; i < laplace->points_number; i++)
    {
        do {
            laplace->points[i].x = rand() % laplace->rows_number;
        } while(laplace->points[i].x + 2 > laplace->rows_number || laplace->points[i].x - 2 < 0);
        do {
            laplace->points[i].y = rand() % laplace->column_number;
        } while(laplace->points[i].y + 2 > laplace->column_number || laplace->points[i].y - 2 < 0);
    }

    setup_initial_temp(vect, laplace);
}

static void prepare_vector(float** vect, struct LaplaceInput* laplace)
{
    for(int i = 0; i < laplace->rows_number; i++)
    {
        for(int j = 0; j < laplace->column_number; j++)
        {
            vect[i][j] = 0;
        }
    }

    switch(laplace->corner)
    {
        case BOTTOM:
            for(int i = 0; i < laplace->column_number; i++)
            {
                vect[laplace->rows_number - 1][i] = laplace->temperature;
            }
            break;
        case TOP:
            for(int i = 0; i < laplace->column_number; i++)
            {
                vect[0][i] = laplace->temperature;
            }
            break;
        case RIGHT:
            for(int i = 0; i < laplace->rows_number; i++)
            {
                vect[i][laplace->column_number - 1] = laplace->temperature;
            }
            break;
        case LEFT:
            for(int i = 0; i < laplace->rows_number; i++)
            {
                vect[i][0] = laplace->temperature;
            }
            break;
        default:
            break;
    }
}

static void print_vector(float** vect, int n, int m)
{
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            printf("%.2lf ", vect[i][j]);
        }
        printf("\n");
    }
}

static void print_vector_char(unsigned char** vect, int n, int m)
{
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            printf("%d ", vect[i][j]);
        }
        printf("\n");
    }
}

static void laplace(float** vect, int n, int m)
{
    for(int i = 1; i < n - 1; i++)
    {
        for(int j = 1; j < m - 1; j++)
        {
            vect[i][j] = 0.25f * (vect[i + 1][j] + vect[i - 1][j] + vect[i][j - 1] + vect[i][j + 1]);
        }
    }
}

static int is_end(float** new_vect, float** old_vect, int n, int m, float eps)
{
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            if( fabs(new_vect[i][j] - old_vect[i][j]) > eps)
            {
                return 1;
            }
        }
    }
    return 0;
}

static void copy_data(float** new_vect, float** old_vect, int n, int m)
{
    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            new_vect[i][j] = old_vect[i][j];
        }
    }
}

static void normalize_to_char(float** vect_float, unsigned char** vect_char, int n, int m, float max, float min)
{

    float** vector = (float**)malloc(sizeof(float*) * n);
    for(int i = 0; i < n; i++)
    {
        vector[i] = (float*)malloc(sizeof(float) * m);
    }

    for(int i = 0; i < n; i++)
    {
        for(int j = 0; j < m; j++)
        {
            vect_char[i][j] = ( (vect_float[i][j] - min) / (max - min) ) * 255;
        }
    }
}

static float** malloc2d_float(int row_size, int column_size)
{
    float** vector = (float**)malloc(row_size * sizeof(float*));
    for(int i = 0; i < row_size; i++)
    {
        vector[i] = (float*)malloc(column_size * sizeof(float));
    }

    return vector;
}

static char** malloc2d_char(int row_size, int column_size)
{
    char** vector = (char**)malloc(row_size * sizeof(char*));
    for(int i = 0; i < row_size; i++)
    {
        vector[i] = (char*)malloc(column_size * sizeof(char));
    }

    return vector;
}

static int parse_int(int argc, char* argv[], char* arg)
{
    int res = -1;
    char* end;
    if(arg == NULL)
    {
        res = strtol(argv[argc], &end, 10);
        if(*end)
        {
            fprintf(stderr, "ERROR: %s has not been recog  nized!\n", argv[argc]);
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        res = strtol(arg, &end, 10);
        if(*end)
        {
            fprintf(stderr, "ERROR: %s has not been recognized!\n", arg);
            exit(EXIT_FAILURE);
        }
    }
    return res;
}

static void parse_arguments(int argc, char* argv[], struct LaplaceInput* lap)
{
    char c;
    memset(lap, 0, sizeof(struct LaplaceInput));

    while(1){
        c = getopt(argc, argv, "n:m:c:p:t:h");
        if(c == -1)
            break;

        switch(c){
            case 'n':
                lap->rows_number = parse_int(optind, argv, optarg);
                break;
            case 'm':
                lap->column_number = parse_int(optind, argv, optarg);
                break;
            case 'c':
                lap->corner = parse_int(optind, argv, optarg);
                break;
            case 'p':
                lap->points_number = parse_int(optind, argv, optarg);
                break;
            case 't':
                lap->temperature = parse_int(optind, argv, optarg);
                break;
            case 'h':
                fprintf(stderr, "Usage: laplace [OPTION]...\nFollowing options are available:\n-n\tIndicates how long horizontally should be calculated matrix.\n-m\tIndicates how long vertically should be calculated matrix.\n-c\tThis parameter determines which corner of matrix should be filled with given temperature. Meaning of values: 1 - TOP, 2 - BOTTOM, 3 - RIGHT, 4 - LEFT\n-p\tOnly if -c parameter has not been chosen - this parameter detrmines how many random points should be picked.\n-t\tIndicates applied temperature.\n");
                exit(EXIT_SUCCESS);
            default:
                break;

        }
    }

    if(lap->rows_number == 0)
    {
        fprintf(stderr, "ERROR: Horizontal size of matrix has not been set!\n Flag to set it: -n\n");
        exit(EXIT_FAILURE);
    }
    if(lap->column_number == 0)
    {
        fprintf(stderr, "ERROR: Vertical size of matrix has not been set!\n Flag to set it: -m\n");
        exit(EXIT_FAILURE);
    }
    if(lap->corner != 0 && lap->points_number != 0)
    {
        fprintf(stderr, "ERROR: Only one of -p and -c parameter should be passed!\n");
        exit(EXIT_FAILURE);
    }

    if(lap->points_number == 0 && lap->corner == 0)
    {
        fprintf(stderr, "ERROR: One of -p and -c parameters should be passed!\nGiven values should be greater than 0!\n");
        exit(EXIT_FAILURE);
    }

    if(lap->temperature == 0)
    {
        fprintf(stderr, "ERROR: Temperature is required argument!\nValue of given temperature should be greater than 0!\n");
        exit(EXIT_FAILURE);
    }

    if(lap->points_number > 0)
    {
        lap->points = (struct Points*)malloc(sizeof(struct Points) * lap->points_number);
        lap->points[0].temperature = 100;
        for(int i = 1; i < lap->points_number; i++)
        {
            lap->points[i].temperature = lap->temperature;
        }
    }

}

static void save_bmp(unsigned char** data, int rows, int col, char* filename, struct Hip* hip)
{
    BMP img;
    img.SetSize(rows, col);
    for(int i = 0; i < rows; i++)
    {
        for(int j = 0; j < col; j++)
        {
            RGBApixel x = get_color(0, 255, data[i][j]);
            x.Alpha = 255;
            img.SetPixel(i, j, x);
        }
    }

    img.SetPixel(hip->point.x, hip->point.y, hip->color);

    img.WriteToFile(filename);
}

int main(int argc, char* argv[])
{
    srand(time(NULL));


    struct LaplaceInput lap;
    struct LaplaceInput initLap;

    struct Hip hip;
    struct Hip initHip;

    parse_arguments(argc, argv, &initLap);

    float** vector = malloc2d_float(initLap.rows_number, initLap.column_number);
    float** vector_new = malloc2d_float(initLap.rows_number, initLap.column_number);


    unsigned char** vector_char = (unsigned char**)malloc2d_char(initLap.rows_number, initLap.column_number);
    int iterator = 0;

    char filename[100];

    initHip.point.x = 5;
    initHip.point.y = 5;
    initHip.color.Red = 255;
    initHip.color.Green = 255;
    initHip.color.Blue = 255;
    initHip.prev_temp = 0;


    int successes = 0;
    for(int j = 0; j < 100; j++)
    {

        memcpy(&lap, &initLap, sizeof(struct LaplaceInput));
        memcpy(&hip, &initHip, sizeof(struct LaplaceInput));
        if(lap.points_number)
        {
            prepare_vector_random(vector, &lap);
        }
        else
        {
            prepare_vector(vector, &lap);
        }
        for(int i = 0; i < 250; i++)
        {
            if(++iterator >= 10)
            {
                move_laplace_points(&lap);
                iterator = 0;
            }
            copy_data(vector_new, vector, lap.rows_number, lap.column_number);
            laplace(vector, lap.rows_number, lap.column_number);
            if(lap.points_number)
            {
                setup_initial_temp(vector, &lap);
            }
            normalize_to_char(vector, vector_char, lap.rows_number, lap.column_number, 100, 0);
//			move_point(&hip.point, lap.rows_number, lap.column_number);
            move_point_hip(&hip, lap.rows_number, lap.column_number, vector);
            if(found_source(&hip, &lap))
            {
                successes++;
                break;
            }
//            if(j == 0)
//            {
//                sprintf(filename, "%d", i);
//                save_bmp(vector_char, lap.rows_number, lap.column_number, filename, &hip);
//            }
        }

    }

    printf("%d\n", successes);
//		normalize_to_char(vector, vector_char, lap.rows_number, lap.column_number, 100, 0);
//		print_vector_char(vector_char, lap.rows_number, lap.column_number);


//	print_vector(vector, lap.rows_number, lap.column_number);
//	print_vector_char(vector_char, lap.rows_number, lap.column_number);

    return 0;
}
