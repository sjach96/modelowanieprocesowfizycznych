#ifndef MODELOWANIEPROCESOWFIZYCZNYCH_NORMS_H
#define MODELOWANIEPROCESOWFIZYCZNYCH_NORMS_H

float sum_absolute_difference(float* x1, float* x2, int elements);
float mean_absolute_difference(float* x1, float* x2, int elements);
float sum_squared_difference(float* x1, float* x2, int elements);
float mean_squared_error(float* x1, float* x2, int elements);

#endif
