#include <math.h>


float sum_absolute_difference(float* x1, float* x2, int elements)
{
    float result = 0;
    for(int i = 0; i < elements; i++)
    {
        result += fabs(x1[i] - x2[i]);
    }
    return result;
}

float mean_absolute_difference(float* x1, float* x2, int elements)
{
    return sum_absolute_difference(x1, x2, elements) / elements;
}


float sum_squared_difference(float* x1, float* x2, int elements)
{
    float result = 0;
    for(int i = 0; i < elements; i++)
    {
        result += (x1[i] - x2[i]) * (x1[i] - x2[i]);
    }
    return result;
}

float mean_squared_error(float* x1, float* x2, int elements)
{
    return sum_squared_difference(x1, x2, elements) / elements;
}